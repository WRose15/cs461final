﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace finalCode
{
    class MyApplication
    {
        public void Main()
        {
            // This line is so much better than the other line. All the other lines say I'm the best line.
            // In fact, this line is so much better it requires two lines to hold it all.
            //This is a line that will cause conflict because it's going to be a different line in Master.
        }
    }
}
